#This is an example script from a Wazuh engineer. Idea would be to push this along with the MSI to the host and 
#kick off this script that calls the MSI. It needs to be run elevated. 

# Parameters for the script
param (

    [switch]$Elevated,

    [string]$package_path = $pwd,
    [string]$package = "wazuh-agent-3.8.2-1.msi",
    [string]$manager = "192.168.3.181",
    [string]$registration = "192.168.3.181",
    [string]$agent_name = "",

    [switch]$help

    )

function Usage
{
    "
       /\__/\
      /      \    WAZUH agent - Windows deploy
      \ \  / /    Site: http://www.wazuh.com
       \ VV /
        \__/
    USE: ./deploy.ps1 [options]
    -package (mandatory): package file.
    -registration (mandatory): hostname or ip used to register the agent.
    -manager (mandatory): hostname or ip of the manager.
    -registration_password (mandatory): registration password.
    -labels (optional): comma separated list of labels. Example: ip=111.222.333.444,db=mysql
    -agent_name (optional): agent name. If not provided, hostname will be used.
    -help: usage information.
    Examples: 
      ./deploy.ps1 -package wazuh-agent-3.2.1-1.msi -manager abc.com -registration def.com -registration_password my_password
    "
}

if(($help.isPresent)) {
    Usage
    Exit
}

# Check agent_name
if ($agent_name -eq "") {
    $agent_name = $env:computername
}

# Check for minimum parameters
if ( $manager -eq "" ) {
    Usage
    Exit
}


if ( $package -eq "" -Or $registration -eq "" ) {
    Usage
    Exit
}

# Opening powershell as Administrator
if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) {
       Write-Host "This script requires Administrator privileges"
       Exit
}

$windows_version = [System.Environment]::OSVersion.Version

"-------------------------------------------------------------------
These are your parameters:
System: $windows_version
Package: $package
Registration: $registration"
"Manager: $manager"
"Agent name: $agent_name"
"-------------------------------------------------------------------
"

# Arguments for msiexec

$argumentList = '/quiet /i {1} /q ADDRESS="{2}" AUTHD_SERVER="{3}"' -f `
    $package_path, `
    $package, `
    $manager, `
    $registration

# Execute installation
"Installing Wazuh Agent and registering"

Start-Process -FilePath "$env:systemroot\system32\msiexec.exe" -Wait -ArgumentList $argumentList

$path = "C:\Program Files (x86)\ossec-agent\"
$log_path = "C:\Program Files (x86)\ossec-agent\ossec.log"

if (Test-Path $path) {
  $version_path = "C:\Program Files (x86)\ossec-agent\VERSION.txt"
} else {
  $version_path = "C:\Program Files\ossec-agent\VERSION.txt"
}

$time = get-date

New-Item -Path $version_path -ItemType File -Force
add-content $version_path -value "Wazuh $package - Installed on $time"

"Installation successful!"

