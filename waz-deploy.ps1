#This script takes a list of hostnames in the .\computers.txt file and loops through each host creating
#a directory, copying the wazuh-agent-3* installer file to that directory, then runs that installer passing
#in the IP of the Wazuh manager. This will only work on systems with Windows Remote Management enabled and
#listening.

#Enter your username and password here. Shred this data afterward.
$Username = ''
$Password = ''
$Pass = ConvertTo-SecureString -AsPlainText $Password -Force
$Cred = New-Object System.Management.Automation.PSCredential -ArgumentList $Username,$pass
$SourceFile = '.\wazuh-agent-3.8.2-1.msi'

#This section will install the software 

foreach ($computer in Get-Content ".\deploy-to.txt") 
{
Invoke-Command -ComputerName $computer -Credential $Cred -ScriptBlock { 
             $NewFolder = 'C:\Wazuh'
             Write-Host "Creating Wazuh Folder on remote machine."
             New-Item -Path $NewFolder -type directory -Force 
             Write-Host "Folder creation complete"
            }
$DestinationFolder = "\\$computer\c$\Wazuh\"
Copy-Item -Path $SourceFile -Destination $DestinationFolder
Write-Host "MSI tranferred to folder"
Invoke-Command -ComputerName $computer -Credential $Cred -ScriptBlock { & cmd /c "msiexec.exe /i c:\Wazuh\wazuh-agent-3.8.2-1.msi" /q ADDRESS="192.168.3.181" AUTHD_SERVER="192.168.3.181" }
Write-Host "Agent Started."
}